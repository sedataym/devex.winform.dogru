﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DogruTestData.Models;
using System.Data.SqlClient;
using Dapper;

namespace DogruTestData
{
    public class CustomerDao
    {
        private string connstr = ConfigurationManager.ConnectionStrings["DogruContext"].ToString();

        public CustomerDao() { }

        public async Task<IEnumerable<Customer>> GetAllCustomers()
        {
            using (var conn = new SqlConnection(connstr))
            {
                var sql = @"select c.*,cities.* from customers as c
                          inner join cities on c.city_id=cities.id;";
                var model = await conn.QueryAsync<Customer, City, Customer>(sql, (customer, city) =>
                {
                    customer.city = city;
                    return customer;
                }, splitOn: "city_id");
                return model.ToList();
            }
        }

        public async Task<Customer> GetCustomer(int customer_id)
        {
            using (var conn = new SqlConnection(connstr))
            {
                var sql = @"select * from customers where id=@customer_id;";
                var model = await conn.QueryFirstAsync<Customer>(sql, new { customer_id = customer_id });
                return model;
            }
        }

        public async Task<int> NewCustomer(Customer customer)
        {
            using (var conn = new SqlConnection(connstr))
            {
                var sql = @"INSERT INTO [dbo].[customers]
                               ([name],[surname],[adress],[city_id],[state_id],[is_active])
                         VALUES
                               (@name,@surname,@adress,@city_id,@state_id,@is_active);SELECT SCOPE_IDENTITY();";
                var model = await conn.ExecuteAsync(sql, customer);
                return model;
            }
        }

        public async Task<int> EditCustomer(Customer customer)
        {
            using (var conn = new SqlConnection(connstr))
            {
                var sql = @"UPDATE [dbo].[customers]
                               SET [name] = @name
                                  ,[surname] = @surname
                                  ,[adress] = @adress
                                  ,[city_id] = @city_id
                                  ,[state_id] = @state_id
                                  ,[is_active] = @is_active
                             WHERE id=@id;";
                var model = await conn.ExecuteAsync(sql, customer);
                return model;
            }
        }

        public async Task<int> DeleteCustomer(int customer_id)
        {
            using (var conn = new SqlConnection(connstr))
            {
                var sql = @"DELETE FROM [dbo].[customers] WHERE id=@customer_id;";
                var model = await conn.ExecuteAsync(sql, new { customer_id = customer_id });
                return model;
            }
        }

        public async Task<IEnumerable<City>> GetCities()
        {
            using (var conn = new SqlConnection(connstr))
            {
                var sql = @"select * from cities;";
                var model = await conn.QueryAsync<City>(sql);
                return model.ToList();
            }
        }

        public async Task<IEnumerable<State>> GetStates(int city_id)
        {
            using (var conn = new SqlConnection(connstr))
            {
                var sql = @"select * from states where city_id=@city_id;";
                var model = await conn.QueryAsync<State>(sql, new { city_id = city_id });
                return model.ToList();
            }
        }
    }
}
