﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogruTestData.Models
{
    public class State
    {
        public int id { get; set; }
        public string name { get; set; }
        public City city { get; set; }
    }
}
