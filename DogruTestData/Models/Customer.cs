﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogruTestData.Models
{
    public class Customer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string adress { get; set; }
        public int city_id { get; set; }
        public int state_id { get; set; }
        public bool is_active { get; set; }
        public City city { get; set; }
        public State state { get; set; }
    }
}
