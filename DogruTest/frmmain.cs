﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DogruTestData;

namespace DogruTest
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void newItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (frmNewItem newItem = new frmNewItem())
            {
                if (newItem.ShowDialog(this) == DialogResult.OK)
                {
                    updateGriddata();
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            updateGriddata();
        }

        private async void updateGriddata()
        {
            CustomerDao cdo = new CustomerDao();
            var customerData = await cdo.GetAllCustomers();
            grdCustomer.DataSource = customerData;
        }


        private void gvCustomer_DoubleClick(object sender, EventArgs e)
        {
            var gridView = sender as GridView;
            int rowId = Convert.ToInt32(gridView.GetFocusedRowCellValue("id"));

            using (frmNewItem newItem = new frmNewItem(rowId))
            {
                if (newItem.ShowDialog(this) == DialogResult.OK)
                {
                    updateGriddata();
                }
            }
        }
    }
}
