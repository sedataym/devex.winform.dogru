﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DogruTestData;
using DevExpress.XtraGrid.Views.Grid;
using DogruTestData.Models;

namespace DogruTest
{
    public partial class frmNewItem : DevExpress.XtraEditors.XtraForm
    {
        int? _customerId;
        public frmNewItem(int? customerId = null)
        {
            InitializeComponent();
            _customerId = customerId;
        }

        private async void frmNewItem_Load(object sender, EventArgs e)
        {
            CustomerDao cdo = new CustomerDao();
            glCities.Properties.DataSource = await cdo.GetCities();

            if (_customerId != null)
            {
                Customer data = await cdo.GetCustomer((int)_customerId);
                txtName.Text = data.name;
                txtSurname.Text = data.surname;
                txtAdress.Text = data.adress;
                glCities.EditValue = data.city_id;
                glStates.EditValue = data.state_id;
                chkActive.Checked = data.is_active;
            }
            else
            {
                btnDelete.Enabled = false;
            }
        }

        private async void glCities_EditValueChanged(object sender, EventArgs e)
        {
            CustomerDao cdo = new CustomerDao();
            var editor = sender as GridLookUpEdit;
            glStates.Properties.DataSource = await cdo.GetStates((int)editor.EditValue);
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            saveCustomer();
        }

        private async void saveCustomer()
        { 
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                Customer data = new Customer();
                data.name = txtName.Text;
                data.surname = txtSurname.Text;
                data.adress = txtAdress.Text;
                data.city_id = Convert.ToInt32(glCities.EditValue);
                data.state_id = Convert.ToInt32(glStates.EditValue);
                data.is_active = chkActive.Checked;

                CustomerDao cdo = new CustomerDao();

                if (_customerId != null)
                {
                    data.id = (int)_customerId;
                    await cdo.EditCustomer(data);
                }
                else
                    await cdo.NewCustomer(data);

                this.DialogResult = DialogResult.OK;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            if (_customerId != null)
            {
                DialogResult _alert = XtraMessageBox.Show("Müşteri kaydını silmek istediğinizden eminmisiniz?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (_alert == DialogResult.Yes)
                {
                    CustomerDao cdo = new CustomerDao();
                    await cdo.DeleteCustomer((int)_customerId);
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                e.Cancel = true;
                txtName.Focus();
                errorProviderApp.SetError(txtName, "Lütfen Adınızı giriniz");
            }
            else
            {
                e.Cancel = false;
                errorProviderApp.SetError(txtName, "");
            }
        }

        private void txtSurname_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSurname.Text))
            {
                e.Cancel = true;
                txtName.Focus();
                errorProviderApp.SetError(txtSurname, "Lütfen Soyadınızı giriniz");
            }
            else
            {
                e.Cancel = false;
                errorProviderApp.SetError(txtSurname, "");
            }
        }

        private void glCities_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(glCities.EditValue.ToString()))
            {
                e.Cancel = true;
                txtName.Focus();
                errorProviderApp.SetError(glCities, "Bir şehir seçiniz");
            }
            else
            {
                e.Cancel = false;
                errorProviderApp.SetError(glCities, "");
            }
        }

        private void glStates_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(glStates.EditValue.ToString()))
            {
                e.Cancel = true;
                txtName.Focus();
                errorProviderApp.SetError(glStates, "Bir şehir seçiniz");
            }
            else
            {
                e.Cancel = false;
                errorProviderApp.SetError(glStates, "");
            }
        }
    }
}